module app.controller.IndexController;

import hunt;

class IndexController : Controller
{
    mixin MakeController;

    @Action
    string index()
    {
        // set huntVersion to view
        view.assign("huntVersion", HUNT_VERSION);
        view.assign("who", request.get("who", "World"));

        return view.render("index");
    }
}
